# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/beblurt/blurt-tools-cmd/compare/v1.0.1...v1.1.0) (2023-11-15)


### Features

* **referral:** add referral ([6e0a82c](https://gitlab.com/beblurt/blurt-tools-cmd/commit/6e0a82c8ec527212fdc9ed543eacfa6e295e5950))

### 1.0.1 (2023-08-01)


### Features

* **init:** init app ([142807d](https://gitlab.com/beblurt/blurt-tools-cmd/commit/142807d5e4b23fd16f29c22d39775fe582221107))
