# Blurt Tools CMD

[Blurt Blockchain](https://beblurt.com/community/blurt-101010) Operation tools in common interactive command line user interfaces ([Inquirer](https://www.npmjs.com/package/inquirer))


## Getting started

To install & start it just do a:

```bash
$ git clone https://gitlab.com/beblurt/blurt-tools-cmd && cd blurt-tools-cmd
~/blurt-tools-cmd$ npm i
~/blurt-tools-cmd$ npm start

> blurt-tools-cmd@1.0.0 start
> tsc && node ./dist/


------------------------------------------
Toolbox for the Blurt Blockchain

? Change the config? (y/N)
```

## Usage

### Change the config?

Upon launching the toolbox, the first prompt will ask if you wish to modify its configuration and if your answer is "Yes" if you want "Reload the default config?" or use a specific configuration. 

The toolbox is already pre-configured for both the main chain and the TESTNET and is open for specific configuration.

### For which chain?

You can then choose to work either on the main Blurt chain or the TESTNET chain.

### What do you want to do?

![image](https://img.blurt.world/blurtimage/nalexadre/0ff4db8e11e6bf21c1addef64628d69a3a9058ee.png)

#### **Create a TESTNET account**

To create an account on the TESTNET, you need to have an account on the main chain and be part of the TESTNET developers' list. To be added, simply request access in the "dev" channel of the official Blurt Discord server: https://discord.blurt.world/

The following accounts are already on the list: @saboin, @tekraze, @sagarkothari88, @eastmael, @khrom, @fervi, @techcoderx, @alejos7ven, @kamranrkploy, @casualfriday (and I might have inadvertently missed some, apologies).

For the questions "What is the BLURT account that will create this username?" and "What is the ACTIVE KEY of the CREATOR account?" please use your main chain account information. 

The key's purpose is only to sign the message sent to the Backend for verification, you can verify this by examining the source code if you're curious. You can use your POSTING key it should work as well.

#### **TESTNET Transfer**

To perform account-to-account transfers on the TESTNET.

#### **TESTNET Power UP**

To execute a Power UP (amount in TESTS) on the TESTNET.

#### **TESTNET Witness Vote**

To vote for a TESTNET witness.

#### **TESTNET Witness set properties**

Set the properties for your TESTNET witness.

---

## Support

You can contact me in the "dev" channel of the Official Blurt Discord server: https://discord.blurt.world/ and you can find some Blurt blockchain useful posts here: [Dev on Blurt community](https://beblurt.com/community/blurt-101010)

## Contributing

Pull requests for new features, bug fixes, and suggestions are welcome!

## Author

@beblurt (https://beblurt.com/@beblurt)

## License

Copyright (C) 2023  IMT ASE Co., LTD

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
