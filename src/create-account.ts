/** 
 * @project blurt-tools-cmd
 * @author  @beblurt (https://blurt.blog/@beblurt)
 * @license
 * Copyright (C) 2023  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'

/** Require */
import Chance from 'chance'
import inquirer, { Answers } from 'inquirer'
import { createHash } from 'crypto'
//const axios = require('axios').default
import axios from 'axios'

/**
* Return sha256 hash of input.
*/
const sha256 = (input: Buffer | string): Buffer => {
  return createHash('sha256')
    .update(input)
    .digest()
}

/** Blockchain */
import { Client, PrivateKey, AccountCreateOperation, AssetSymbol } from '@beblurt/dblurt'
type KEYS = {
  owner:   { private: string, public: string }
  active:  { private: string, public: string }
  posting: { private: string, public: string }
  memo:    { private: string, public: string }
}

/** Blurt account format verification function */
import { communityAccountRegexp, cryptoUtils, NexusUpdateProps } from '@beblurt/dblurt'
const isUsernameValid = async (value: string): Promise<boolean> => {
  try {
    assert(value !== undefined && value !== null && value.toString().length !== 0, `${value} can't be tested!`)
    let result = false
    const re = new RegExp(cryptoUtils.regExpAccount)
    result = re.test(value)
    if(result) {
      return true
    } else {
      return false
    } 
  } catch (e) {
    return false
  }
}
const isCommunityValid = async (value: string): Promise<boolean> => {
  try {
    assert(value !== undefined && value !== null && value.toString().length !== 0, `${value} can't be tested!`)
    let result = false
    const re = new RegExp(communityAccountRegexp)
    result = re.test(value)
    if(result) {
      return true
    } else {
      return false
    } 
  } catch (e) {
    return false
  }
}
const isUsernameUnique = async (value: string, client: Client): Promise<string> => {
  let msg = await isUsernameValid(value) ? 'valid' : 'Invalid Blurt username'
  if(msg === 'valid') {
    const accounts = await client.condenser.getAccounts([value])
    if(accounts[0]) {
      msg = 'Username already exists'
    }
  }
  return msg
}
const isUsernameExist = async (value: string, client: Client): Promise<string> => {
  let msg = await isUsernameValid(value) ? 'valid' : 'Invalid Blurt username'
  if(msg === 'valid') {
    const accounts = await client.condenser.getAccounts([value])
    if(!accounts[0]) {
      msg = 'Invalid Blurt username'
    }
  }
  return msg
}

export const CreateAccount = async (config: { ADDRESS_PREFIX: string; CHAIN_ID: string; NODES_RPC: string[], ASSET: AssetSymbol }, testnet?: boolean): Promise<void> => {
  try {
    /** Create new Client */
    const client = new Client(config.NODES_RPC, { addressPrefix: config.ADDRESS_PREFIX, chainId: config.CHAIN_ID })

    const answers: Answers = await inquirer.prompt([
      {
        type: 'input',
        name: 'username',
        message: 'What BLURT USERNAME would you like to have?',
        async validate(value) {
          const msg = await isUsernameUnique(value, client);
          if(msg === 'valid') {
            return true;
          }
    
          return msg;
        },
      },
      {
        type: 'confirm',
        name: 'community',
        message: 'Is it a Community account?',
        default: false,
        when: () => !testnet,
      },
      {
        type: 'input',
        name: 'creatorAccount',
        message: 'What is the BLURT MAIN CHAIN account that will create this username?',
        validate(value) {
          const pass = value.match(
            cryptoUtils.regExpAccount
          );
          if (pass) {
            return true;
          }
    
          return 'Invalid creator account account';
        },
      },
      {
        type: 'password',
        name: 'creatorAccountActiveKey',
        message: 'What is the ACTIVE KEY of the CREATOR MAIN CHAIN account?',
      },
      {
        type: 'confirm',
        name: 'affiliate',
        message: 'There is a referrer account?',
        default: false,
      },
      {
        type: 'input',
        name: 'referrer',
        message: 'What is the REFERRER account?',
        when: (answers) => answers.affiliate,
        async validate(value) {
          const msg = await isUsernameExist(value, client);
          if(msg === 'valid') {
            return true;
          }
    
          return msg;
        },
      },
      {
        type: 'input',
        name: 'campaign_id',
        message: 'What is the CAMPAIGN ID? (optional)',
        when: (answers) => answers.affiliate,
      },
    ])

    /** If community */
    if(answers['community']) {
      assert(isCommunityValid(answers['username']), `${answers['username']} is not a valid community account!`) 
    } else {
      assert(isUsernameValid(answers['username']), `${answers['username']} is not a valid username!`)
    }

    /** Generate random hash password */
    const chance   = new Chance()
    const password = chance.hash({ length: 32 })

    /** Call the create function */
    const username = answers['username']
    const creatorAccount = answers['creatorAccount']
    const creatorAccountActiveKey = answers['creatorAccountActiveKey']

    /** Fees */
    const chainProperties = await client.condenser.getChainProperties()
    console.log()
    console.log('\x1b[1;35m%s\x1b[0m', `account_creation_fee:`, chainProperties.account_creation_fee)
    const fees: Answers = await inquirer.prompt([
      {
        type: 'confirm',
        name: 'account_creation_fee',
        message: `Account creation fees is ${chainProperties.account_creation_fee} Continue?`,
        default: true,
      },
    ])
    assert(fees['account_creation_fee'], `Process aborted by the user!`)

    /** Keys */
    const masterPassword = sha256(password)
    console.log('\x1b[1;33m%s\x1b[0m', `${username} master password:`, masterPassword.toString('hex'))

    const ownerKey   = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'owner')
    const activeKey  = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'active')
    const postingKey = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'posting')
    const memoKey    = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'memo')

    const keys: KEYS = {
      owner: { 
        private: ownerKey.toString(), 
        public:  ownerKey.createPublic(config.ADDRESS_PREFIX).toString() 
      },
      active:  { 
        private: activeKey.toString(), 
        public:  activeKey.createPublic(config.ADDRESS_PREFIX).toString() 
      },
      posting: { 
        private: postingKey.toString(), 
        public:  postingKey.createPublic(config.ADDRESS_PREFIX).toString() 
      },
      memo:    { 
        private: memoKey.toString(), 
        public:  memoKey.createPublic(config.ADDRESS_PREFIX).toString() 
      },
    }
    console.log({ keys })

    /** Prepare Blurt Blockchain Operation */
    const json_metadata = {
      profile: {
        name: username,
        about: `@${username}`,
        website: '',
        cover_image:   '',
        profile_image: ''
      },
      app: `blurt-tools-cmd/${process.env['VERSION']}`
    }

    const op: AccountCreateOperation = [
      'account_create',
      {
        creator: creatorAccount,
        new_account_name: username,
        fee: chainProperties.account_creation_fee,
        owner: {
          weight_threshold: 1,
          account_auths: [],
          key_auths: [[ keys.owner.public, 1 ]],
        },
        active: {
          weight_threshold: 1,
          account_auths: [],
          key_auths: [[ keys.active.public, 1 ]],
        },
        posting: {
          weight_threshold: 1,
          account_auths: [],
          key_auths: [[ keys.posting.public, 1 ]],
        },
        memo_key:      keys.memo.public,
        json_metadata: JSON.stringify(json_metadata),
        // extensions:    [],
      }
    ]    
    const keyActive = PrivateKey.fromString(creatorAccountActiveKey)

    /** If testnet relay to serverside */
    if(testnet) {
      const msg = sha256(JSON.stringify(op))
      const signature = keyActive.sign(Buffer.from(msg)).toString()
      const response = await axios.post('https://beblurt.com/rpc', {
        jsonrpc: '2.0',
        method:  'testnet.create_account',
        params:  { op, signature },
        id:      1
      })
      assert(response.data, `No data!`)
      assert(!response.data.error, response.data.error?.message)
      console.log('[response]', response.data.result)
    } else {
      /** Broadcast Operation to the blockchain */
      const response = await client.broadcast.sendOperations([op], keyActive)
      console.log('[response]', response)
    }

    console.log('\x1b[\x1b[1;36m%s\x1b[0m', 'account created with success:', answers['username'])

    /** If affiliate account */
    if(answers['affiliate']) {
      // const op: CustomJsonOperation = [
      //   'custom_json',
      //   {
      //     required_auths: [],
      //     required_posting_auths: [username],
      //     id: 'referral',
      //     json: JSON.stringify({ referrer: answers['referrer'], campaign: answers['campaign_id'] })
      //   }
      // ]
      // /** Broadcast Referrer Operation to the blockchain (posting key) */
      // const response = await client.broadcast.sendOperations([op], postingKey)
      // console.log('[Referrer Operation]', response)
      /** Broadcast Referrer Operation to the blockchain (posting key) */
      const response = await client.broadcast.nexusAddReferrer(username, answers['referrer'], answers['campaign_id'], postingKey)
      console.log('[Referrer Operation]', response)
    }

    /** If community */
    if(answers['community']) {
      console.log()
      console.log('########################################')
      console.log('## Transfer some BLURT')
      console.log('## to the new Community account')
      console.log('## for the fees')
      console.log('## BEFORE TO CONTINUE !')
      console.log('########################################')

      const community: Answers = await inquirer.prompt([
        {
          type: 'input',
          name: 'title',
          message: 'Title of this community (32 chars maximum)',
        },
        {
          type: 'input',
          name: 'about',
          message: 'about this community (120 chars maximum)',
        },
        {
          type: 'input',
          name: 'lang',
          message: 'lang of this community (ISO 639-1)',
          default: 'en',
        },
        {
          type: 'confirm',
          name: 'is_nsfw',
          message: 'Is this community 18+ (NSFW)?',
          default: false
        },
        {
          type: 'input',
          name: 'description',
          message: 'description of this community',
        },
        {
          type: 'input',
          name: 'flag_text',
          message: 'custom text for reporting content of this community',
        },
        {
          type: 'input',
          name: 'avatar_url',
          message: 'avatars URL',
        },
        {
          type: 'input',
          name: 'cover_url',
          message: 'covers URL',
        },
        {
          type: 'list',
          name: 'default_view',
          message: 'Default view of this community',
          choices: [ 'list', 'blog', 'grid' ],
          default: 'grid'
        },
      ])

      const props: NexusUpdateProps = {
        community: answers['username'],
        props: {
          title:       community['title'],
          about:       community['about'],
          lang:        community['lang'],
          is_nsfw:     community['is_nsfw'],
          description: community['description'],
          flag_text:   community['flag_text'],
          settings: {
            avatar_url: community['avatar_url'],
            cover_url:  community['cover_url'],
            default_view:  community['default_view'],
          }
        }
      }
      const responseUpdateProps = await client.broadcast.nexusUpdateProps(props, answers['username'], postingKey)
      console.log('[response Update Props]', responseUpdateProps)
    }
    return
  } catch (e) {
    throw e
  }
}