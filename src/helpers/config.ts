/** 
 * @project blurt-tools-cmd
 * @author  @beblurt (https://blurt.blog/@beblurt)
 * @license
 * Copyright (C) 2023  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import Joi from 'joi'

import { AssetSymbol } from '@beblurt/dblurt'

export type CONFIG = {
  TESTNET: {
    ADDRESS_PREFIX: string // TST
    CHAIN_ID:       string // 1df54a5cc86f7c7efee2402e1304df6eae24eb8766a63c0546c1b2511cf5eba6
    NODES_RPC:      string[] // List of RPC nodes
    ASSET:          AssetSymbol
  },
  BLURT: {
    ADDRESS_PREFIX: string // BLT
    CHAIN_ID:       string // cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f
    NODES_RPC:      string[] // List of RPC nodes
    ASSET:          AssetSymbol
  }
}

const schemaConfig = Joi.object({
  TESTNET: Joi.object({
    ADDRESS_PREFIX: Joi.string().required(),
    CHAIN_ID:       Joi.string().required(),
    NODES_RPC:      Joi.array().items(Joi.string()).required(),
    ASSET:          Joi.string().required(),
  }),
  BLURT: Joi.object({
    ADDRESS_PREFIX: Joi.string().required(),
    CHAIN_ID:       Joi.string().required(),
    NODES_RPC:      Joi.array().items(Joi.string()).required(),
    ASSET:          Joi.string().required(),
  })
})

export const checkConfig = async (source: CONFIG): Promise<CONFIG> => {
  try {
    return await schemaConfig.validateAsync(source)
  } catch (e) {
    throw e
  }
}