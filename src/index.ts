/** 
 * @project blurt-tools-cmd
 * @author  @beblurt (https://blurt.blog/@beblurt)
 * @license
 * Copyright (C) 2023  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { createRequire } from "module"
const require          = createRequire(import.meta.url)
const packagejson      = require('../package.json')
process.env['VERSION'] = packagejson.version

import fs from 'fs';
const { readFile, writeFile } = fs.promises

/** Helpers */
import { CONFIG, checkConfig } from './helpers/config.js'
const default_blurt_config: CONFIG = {
  TESTNET: {
    ADDRESS_PREFIX: 'TST',
    CHAIN_ID:       '1df54a5cc86f7c7efee2402e1304df6eae24eb8766a63c0546c1b2511cf5eba6',
    NODES_RPC:      ['https://testnet-rpc.beblurt.com'],
    ASSET:          'TESTS'
  },
  BLURT: {
    ADDRESS_PREFIX: 'BLT',
    CHAIN_ID:       'cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f',
    NODES_RPC:      ['https://rpc.beblurt.com','https://rpc.blurt.world'],
    ASSET:          'BLURT'
  }
}
const getBlurtConfig = async (): Promise<CONFIG|null> => {
  return readFile(new URL('../blurt_config.json', import.meta.url)).then(async bufferBlurtConfig => {
    const blurt_config: CONFIG  = JSON.parse(bufferBlurtConfig.toString())
    await checkConfig(blurt_config)
    return blurt_config
  }).catch (() => { return null })
}
const saveBlurtConfig = async (blurt_config: CONFIG): Promise<CONFIG> => {
  try {
    await writeFile(new URL('../blurt_config.json', import.meta.url), JSON.stringify(blurt_config))
    return blurt_config
  } catch (e) {
    throw e
  }
}
const changeBlurtConfig = async (blurt_config: CONFIG|null): Promise<CONFIG> => {
  try {
    const config: Answers = await inquirer.prompt([
      {
        type: 'input',
        name: 'TESTNET_ADDRESS_PREFIX',
        message: 'TESTNET_ADDRESS_PREFIX',
        default: blurt_config ? blurt_config.TESTNET.ADDRESS_PREFIX : default_blurt_config.TESTNET.ADDRESS_PREFIX,
      },
      {
        type: 'input',
        name: 'TESTNET_CHAIN_ID',
        message: 'TESTNET_CHAIN_ID',
        default: blurt_config ? blurt_config.TESTNET.CHAIN_ID : default_blurt_config.TESTNET.CHAIN_ID,
      },
      {
        type: 'input',
        name: 'TESTNET_NODES_RPC',
        message: 'TESTNET_NODES_RPC',
        default: blurt_config ? blurt_config.TESTNET.NODES_RPC : default_blurt_config.TESTNET.NODES_RPC,
      },
      {
        type: 'input',
        name: 'TESTNET_ASSET',
        message: 'TESTNET_ASSET',
        default: blurt_config ? blurt_config.TESTNET.ASSET : default_blurt_config.TESTNET.ASSET,
      },
      {
        type: 'input',
        name: 'BLURT_ADDRESS_PREFIX',
        message: 'BLURT_ADDRESS_PREFIX',
        default: blurt_config ? blurt_config.BLURT.ADDRESS_PREFIX : default_blurt_config.BLURT.ADDRESS_PREFIX,
      },
      {
        type: 'input',
        name: 'BLURT_CHAIN_ID',
        message: 'BLURT_CHAIN_ID',
        default: blurt_config ? blurt_config.BLURT.CHAIN_ID : default_blurt_config.BLURT.CHAIN_ID,
      },
      {
        type: 'input',
        name: 'BLURT_NODES_RPC',
        message: 'BLURT_NODES_RPC',
        default: blurt_config ? blurt_config.BLURT.NODES_RPC : default_blurt_config.BLURT.NODES_RPC,
      },
      {
        type: 'input',
        name: 'BLURT_ASSET',
        message: 'BLURT_ASSET',
        default: blurt_config ? blurt_config.BLURT.ASSET : default_blurt_config.BLURT.ASSET,
      },
    ])
    return await saveBlurtConfig({
      TESTNET: {
        ADDRESS_PREFIX: config['TESTNET_ADDRESS_PREFIX'],
        CHAIN_ID:       config['TESTNET_CHAIN_ID'],
        NODES_RPC:      [config['TESTNET_NODES_RPC']],
        ASSET:          config['TESTNET_ASSET'],
      },
      BLURT: {
        ADDRESS_PREFIX: config['BLURT_ADDRESS_PREFIX'],
        CHAIN_ID:       config['BLURT_CHAIN_ID'],
        NODES_RPC:      [config['BLURT_NODES_RPC']],
        ASSET:          config['BLURT_ASSET'],
      }
    })
  } catch (e) {
    throw e
  }
}

/** Require */
import inquirer, { Answers } from 'inquirer'

/** Cases */
import { CreateAccount }        from './create-account.js'
import { Transfer }             from './transfer.js'
import { TransferToVesting }    from './transfer-to-vesting.js'
import { WitnessSetProperties } from './witness-set-properties.js'
import { WitnessVote }          from './witness-vote.js'

console.log()
console.log('\x1b[1;33m%s\x1b[0m', '------------------------------------------')
console.log('\x1b[1;33m%s\x1b[0m', `Toolbox for the Blurt Blockchain`)
console.log()

const start = async (): Promise<void> => {
  try {
    /** Check if Blurt config exist */
    let blurt_config = await getBlurtConfig()

    /** create Blurt config first */
    if(!blurt_config) {
      blurt_config = await saveBlurtConfig(default_blurt_config)
    }

    /** Config menu */
    const configChange: Answers = await inquirer.prompt([
      {
        type: 'confirm',
        name: 'change_config',
        message: 'Change the config?',
        default: false,
      },
      {
        type: 'confirm',
        name: 'reload_default_config',
        message: 'Reload the default config?',
        default: false,
        when: (answers) => answers.change_config,
      }
    ])

    /** If change Config */
    if(configChange['change_config']) {
      if(configChange['reload_default_config']) {
        blurt_config = await saveBlurtConfig(default_blurt_config)
      } else {
        blurt_config = await changeBlurtConfig(blurt_config)
      }
    }

    /** Main menu */
    const main: Answers = await inquirer.prompt([
      {
        type: 'list',
        name: 'ENVIRONMENT',
        message: 'For which chain?',
        choices: ['BLURT', 'TESTNET'],
        default: 'TESTNET'
      }
    ])    

    switch (main['ENVIRONMENT']) {
      case 'BLURT':
        const blurt: Answers = await inquirer.prompt([
          {
            type: 'confirm',
            name: 'debugMode',
            message: 'Run in debug mode?',
            default: false,
          },
          {
            type: 'list',
            name: 'action',
            message: 'What do you want to do?',
            choices: ['Create a Blurt account', 'Blurt transfer', 'Blurt Power UP', 'Blurt Witness Vote', 'Blurt Witness set properties'],
          }
        ])
        switch (blurt['action']) {
          case 'Create a Blurt account':
            await CreateAccount(blurt_config!.BLURT)
            break

          case 'Blurt transfer':
            await Transfer(blurt_config!.BLURT)
            break   
            
          case 'Blurt Power UP':
            await TransferToVesting(blurt_config!.BLURT)
            break              

          case 'Blurt Witness Vote':
            await WitnessVote(blurt_config!.BLURT)
            break 

          case 'Blurt Witness set properties':
            await WitnessSetProperties(blurt_config!.BLURT)
            break            
    
          default:
            break        
        }
        break

      case 'TESTNET':
        const testnet: Answers = await inquirer.prompt([
          {
            type: 'confirm',
            name: 'debugMode',
            message: 'Run in debug mode?',
            default: true,
          },
          {
            type: 'list',
            name: 'action',
            message: 'What do you want to do?',
            choices: ['Create a TESTNET account', 'TESTNET transfer', 'TESTNET Power UP', 'TESTNET Witness Vote', 'TESTNET Witness set properties'],
          }
        ])
        switch (testnet['action']) {
          case 'Create a TESTNET account':
            await CreateAccount(blurt_config!.TESTNET, true)
            break 

          case 'TESTNET transfer':
            await Transfer(blurt_config!.TESTNET, true)
            break   
            
          case 'TESTNET Power UP':
            await TransferToVesting(blurt_config!.TESTNET, true)
            break              

          case 'TESTNET Witness Vote':
            await WitnessVote(blurt_config!.TESTNET, true)
            break 

          case 'TESTNET Witness set properties':
            await WitnessSetProperties(blurt_config!.TESTNET, true)
            break 
        }
        break
      
      default:
        break       
    }

  } catch (e) {
    if (e instanceof Error) {
      console.log('\x1b[31m%s\x1b[0m', `Error: ${e.message}`)
      if (process.env['APP_DEBUG']) console.log('\x1b[33m%s\x1b[0m', 'Stack:', e.stack ? e.stack : 'No info')
    } else {
      if (process.env['APP_DEBUG']) console.log(e)
    }
  }
}
start()