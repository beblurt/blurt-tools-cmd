/** 
 * @project blurt-tools-cmd
 * @author  @beblurt (https://blurt.blog/@beblurt)
 * @license
 * Copyright (C) 2023  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Require */
import inquirer, { Answers } from 'inquirer'

/** Blockchain */
import { AssetSymbol, cryptoUtils, Client, PrivateKey, Asset, TransferToVestingOperation } from '@beblurt/dblurt'

export const TransferToVesting = async (config: { ADDRESS_PREFIX: string; CHAIN_ID: string; NODES_RPC: string[], ASSET: AssetSymbol }, testnet?: boolean): Promise<void> => {
  try {
    const answers: Answers = await inquirer.prompt([
      {
        type: 'input',
        name: 'from',
        message: 'From account? (without @)',
        validate(value) {
          const pass = value.match(
            cryptoUtils.regExpAccount
          );
          if (pass) {
            return true;
          }
    
          return 'Invalid sender account';
        },
      },
      {
        type: 'password',
        name: 'fromAccountActiveKey',
        message: 'What is the ACTIVE KEY?',
      },
      {
        type: 'input',
        name: 'amount',
        message: `What is the amount? (in ${config.ASSET})`,
        validate(value) {
          const pass = value.match(
            /^[1-9]\d*(\.\d+)?$/
          );
          if (pass) {
            return true;
          }
    
          return 'Invalid amount';
        },
      },
      {
        type: 'input',
        name: 'to',
        message: 'to account? (without @)',
        validate(value) {
          const pass = value.match(
            cryptoUtils.regExpAccount
          );
          if (pass) {
            return true;
          }
    
          return 'Invalid sender account';
        },
      },
    ])

    const amount = Asset.from(Number(answers['amount']), config.ASSET)

    const verify: Answers = await inquirer.prompt([
      {
        type: 'confirm',
        name: 'confirm',
        message: `${testnet ? 'TESTNET ' : ''}Power Up ${amount.toString()} from ${answers['from']} to ${answers['to']}?`,
        default: true
      },
    ])

    if(verify['confirm']) {
      /** Create new Client */
      const client = new Client(config.NODES_RPC, { addressPrefix: config.ADDRESS_PREFIX, chainId: config.CHAIN_ID })

      const op: TransferToVestingOperation = [
        'transfer_to_vesting',
        {
          from: answers['from'],
          to: answers['to'],
          amount: amount,
        }
      ]

      const privateActiceKey = PrivateKey.from(answers['fromAccountActiveKey'])
      const response = await client.broadcast.sendOperations([op], privateActiceKey)
      console.log('[response]', response)
      return
    } else {
      console.log('Operation cancelled!')
      return
    }    
  } catch (e) {
    throw e
  }
}
