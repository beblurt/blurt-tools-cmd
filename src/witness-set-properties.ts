/** 
 * @project blurt-tools-cmd
 * @author  @beblurt (https://blurt.blog/@beblurt)
 * @license
 * Copyright (C) 2023  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Require */
import inquirer, { Answers } from 'inquirer'

/** Blockchain */
import { cryptoUtils, Client, PrivateKey, Asset, AssetSymbol, Tools } from '@beblurt/dblurt'

export const WitnessSetProperties = async (config: { ADDRESS_PREFIX: string; CHAIN_ID: string; NODES_RPC: string[], ASSET: AssetSymbol }, testnet?: boolean): Promise<void> => {
  try {
    const answers: Answers = await inquirer.prompt([
      {
        type: 'input',
        name: 'witness',
        message: 'witness account? (without @)',
        validate(value) {
          const pass = value.match(
            cryptoUtils.regExpAccount
          );
          if (pass) {
            return true;
          }
    
          return 'Invalid sender account';
        },
      },
      {
        type: 'password',
        name: 'witnessAccountSigningKey',
        message: 'What is the PRIVATE SIGNING KEY of the witness account?',
      },
      {
        type: 'input',
        name: 'account_creation_fee',
        message: 'What is the amount of the Account creation fee?',
        default: 10,
      },
      {
        type: 'input',
        name: 'operation_flat_fee',
        message: 'What is the amount of the Operation flat fee?',
        default: 0.001,
      },
      {
        type: 'input',
        name: 'bandwidth_kbytes_fee',
        message: 'What is the amount of the Bandwidth fee per Kbytes?',
        default: 0.500,
      },
      {
        type: 'input',
        name: 'proposal_fee',
        message: 'What is the amount of the DAO proposal fee?',
        default: 2000,
      },
      {
        type: 'input',
        name: 'url',
        message: 'What is the URL of the Witness post announcement?',
        validate(value) {
          if (value.length > 0) {
            return true;
          }
    
          return 'Value required';
        },
      },
      {
        type: 'input',
        name: 'maximum_block_size',
        message: 'What is the maximum block size?',
        default: 131072,
      },
      {
        type: 'input',
        name: 'account_subsidy_budget',
        message: 'What is the account subsidy budget?',
        default: 797,
      },
      {
        type: 'input',
        name: 'account_subsidy_decay',
        message: 'What is the account subsidy decay?',
        default: 347321,
      },            
    ])

    const account_creation_fee = Asset.from(Number(answers['account_creation_fee']), config.ASSET)
    const operation_flat_fee   = Asset.from(Number(answers['operation_flat_fee']), config.ASSET)
    const bandwidth_kbytes_fee = Asset.from(Number(answers['bandwidth_kbytes_fee']), config.ASSET)
    const proposal_fee         = Asset.from(Number(answers['proposal_fee']), config.ASSET)

    const verify: Answers = await inquirer.prompt([
      {
        type: 'confirm',
        name: 'confirm',
        message: `${testnet ? 'TESTNET ' : ''}Witness Set properties Account creation fee: ${account_creation_fee.toString()}, Operation flat fee: ${operation_flat_fee.toString()}, ?`,
        default: true
      },
    ])

    if(verify['confirm']) {
      /** Create new Client */
      const client = new Client(config.NODES_RPC, { addressPrefix: config.ADDRESS_PREFIX, chainId: config.CHAIN_ID })

      const key = PrivateKey.from(answers['witnessAccountSigningKey'])

      const dblurtTools = new Tools(client)
      const witnessSetPropertiesOperation = dblurtTools.buildWitnessSetPropertiesOp(answers['witness'], {
        account_creation_fee,
        maximum_block_size:     Number(answers['maximum_block_size']),
        account_subsidy_budget: Number(answers['account_subsidy_budget']),
        account_subsidy_decay:  Number(answers['account_subsidy_decay']),
        operation_flat_fee,
        bandwidth_kbytes_fee,
        proposal_fee,
        url: answers['url'],
        key: key.createPublic(config.ADDRESS_PREFIX),
      })
      
      const response = await client.broadcast.sendOperations([witnessSetPropertiesOperation], key)
      console.log('[response]', response)

    }
  } catch (e) {
    throw e
  }
}    